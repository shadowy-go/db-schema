package schema

/**
Db function
 */
type Function struct {
	Description *string                   `yaml:"description"`	//Description
	Params      *map[string]FunctionParam `yaml:"params"`		//Params
	Return      *string                   `yaml:"return"`		//Return
	Body        *string                   `yaml:"body"`			//Body
	Lang        *string                   `yaml:"lang"`			//Language
}
