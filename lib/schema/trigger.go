package schema

/**
Trigger
 */
type Trigger struct {
	Event  string        `yaml:"event"`		//Event
	Timing string        `yaml:"timing"`	//Timing before, after
	Object string        `yaml:"object"`	//Object row, ...
	Action TriggerAction `yaml:"action"`	//Action
}

/**
Trigger action
 */
type TriggerAction struct {
	Schema    string `yaml:"schema"`		//Schema
	Procedure string `yaml:"procedure"`		//Function
}
