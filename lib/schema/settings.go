package schema

/*
Setting of utils
 */
type Settings struct {
	SkipSchema *[]string `yaml:"skipSchema"` //Schemas what should be skipped
	File       *string   `yaml:"file"`		 //File
}
