package schema

/**
	DB Schema
 */
type Schema struct {
	Sequences *map[string]Sequence `yaml:"sequences"` 	//Sequences
	Types     *Type                `yaml:"types"`		//User define types
	Tables    *map[string]Table    `yaml:"tables"`		//Tables
	Functions *map[string]Function `yaml:"functions"`	//Functions
}
