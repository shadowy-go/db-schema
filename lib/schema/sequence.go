package schema

/**
Sequence
 */
type Sequence struct {
	Type  *string  `yaml:"type"`	//Type
	Start *float64 `yaml:"start"`	//Start
	Min   *float64 `yaml:"min"`		//Min
	Max   *float64 `yaml:"max"`		//Max
	Step  *float64 `yaml:"step"`	//Step
}
