package schema

/**
Table index
 */
type TableIndex struct {
	Uniq    *bool    `yaml:"uniq"`		//is uniq key
	Columns []string `yaml:"columns"`	//columns
}
