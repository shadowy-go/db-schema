package schema

import (
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"gitlab.com/shadowy-go/common"
	"gopkg.in/yaml.v2"
)

/**
Structure of DB
 */
type Db struct {
	Settings Settings `yaml:"settings"` // Setting of utils
	Struct   DbStruct `yaml:"db"`		// Db Struct
}

/**
Load from file
 */
func (db *Db) LoadFromFile(filename string) error {
	logrus.WithField("filename", filename).Info("schema.Db.LoadFromFile")
	//Read files
	var data, err = ioutil.ReadFile(common.GetAbsolutePath(filename))
	if err != nil {
		logrus.WithError(err).Error("Settings.LoadFromFile read")
		return err
	}
	//Parse data
	err = yaml.Unmarshal(data, db)
	if err != nil {
		logrus.WithError(err).Error("Settings.LoadFromFile parse")
		return err
	}
	return nil
}

/**
Save to file
 */
func (db Db) SaveToFile(filename string) error {
	logrus.WithField("filename", filename).Info("schema.Db.SaveToFile")
	//Serialize data
	data, err := yaml.Marshal(db)
	if err != nil {
		logrus.WithError(err).Error("Settings.SaveToFile serialize")
		return err
	}
	//Write to file
	err = ioutil.WriteFile(common.GetAbsolutePath(filename), data, 0x0666)
	if err != nil {
		logrus.WithError(err).Error("Settings.SaveToFile write")
		return err
	}
	return nil
}