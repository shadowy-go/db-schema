package schema

import (
	"flag"
	"path"
	"runtime"
	"testing"
)

func setRootPath() {
	_, filename, _, _ := runtime.Caller(0)
	root, _ := path.Split(filename)
	flag.Set("root", root)
}

func TestDb_LoadFromFile(t *testing.T) {
	setRootPath()
	var db = Db{}
	err := db.LoadFromFile("../../test-data/test.yaml")
	if err != nil {
		t.Error("Error read file")
	}
}

func TestDb_LoadFromFileFail(t *testing.T) {
	setRootPath()
	var db = Db{}
	err := db.LoadFromFile("../../test-data/test1.yaml")
	if err == nil {
		t.Error("No error when read file")
	}
}

func TestDb_LoadFromFileFailParse(t *testing.T) {
	setRootPath()
	var db = Db{}
	err := db.LoadFromFile("../../test-data/test-error.yaml")
	if err == nil {
		t.Error("No error when read file")
	}
}

func TestDb_WriteToFile(t *testing.T) {
	setRootPath()
	var db = Db{}
	err := db.LoadFromFile("../../test-data/test.yaml")
	if err != nil {
		t.Error("Error read file")
	}
	err = db.SaveToFile("../../test-data/test-write.yaml")
	if err != nil {
		t.Error("Error write file")
	}
}

func TestDb_WriteToFileFail(t *testing.T) {
	setRootPath()
	var db = Db{}
	err := db.LoadFromFile("../../test-data/test.yaml")
	if err != nil {
		t.Error("Error read file")
	}
	err = db.SaveToFile("/---/-------")
	if err == nil {
		t.Error("Error not fired when save file")
	}
}
