package schema

/**
Db structure
 */
type DbStruct struct {
	Schema     *map[string]Schema `yaml:"schema"`		//Schema of DB
	Extensions []string           `yaml:"extensions"`	//Extensions for DB
}
