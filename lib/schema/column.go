package schema

/**
Table column
 */
type Column struct {
	Name     string   `yaml:"name"` 	//Name of column
	Type     string   `yaml:"type"`		//Type of column
	Length   *float64 `yaml:"length"`	//Length of column
	Nullable *bool    `yaml:"nullable"` //Is nullable column
	Default  *string  `yaml:"default"`	//Default value
}
