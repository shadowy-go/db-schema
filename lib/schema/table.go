package schema

/**
Db Table
 */
type Table struct {
	Columns  []Column               `yaml:"columns"`	//Columns
	Primary  *map[string][]string   `yaml:"primary"`	//Primary key
	Indexes  *map[string]TableIndex `yaml:"indexes"`	//Indexes
	Triggers *map[string]Trigger    `yaml:"triggers"`	//Triggers
}
