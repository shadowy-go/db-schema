package schema

/**
	Type of field
 */
type TypeField struct {
	Name   string  `yaml:"name"`  	//Name
	Schema *string `yaml:"schema"`	//Schema
	Type   string  `yaml:"type"`	//Type
}

/**
Custom type
 */
type Type map[string][]TypeField
