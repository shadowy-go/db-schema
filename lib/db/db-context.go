package db

import (
	"flag"
	"fmt"
	"database/sql"
	"github.com/sirupsen/logrus"
)

/**
Db context
 */
type Context struct {
	host             string		//host
	port             int		//port
	user             string		//user
	password         string		//password
	db               string		//db
	sslMode          string		//ssl mode
	connectionString string		//connection string
}

//Current Db context
var CurrentDbContext = getDbContext()

/**
Db context factory
 */
func getDbContext() *Context {
	var result = Context{}
	result.init()
	return &result
}

/**
Command line flags
 */
var flagHost = flag.String("host", "localhost", "")
var flagPort = flag.Int("port", 5432, "")
var flagUser = flag.String("user", "postgres", "")
var flagPassword = flag.String("password", "", "")
var flagDb = flag.String("db", "", "")
var flagSslMode = flag.String("sslMode", "disable", "")

/**
  Callback function for receiving db connection
*/
type DBContextFunction func(*sql.DB) error

/**
  Callback function for receiving db transaction
*/
type DBContextWithTransactionFunction func(*sql.Tx) error

/**
  Callback function for apply field mapping
*/
type DBFieldMapping func(*sql.Row) (interface{}, error)

/**
Initialize context
 */
func (context *Context) init() {
	context.host = *flagHost
	context.port = *flagPort
	context.user = *flagUser
	context.password = *flagPassword
	context.db = *flagDb
	context.sslMode = *flagSslMode
	context.connectionString = fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=%s", context.user,
		context.password, context.host, context.port, context.db, context.sslMode)
}

/**
Get connection for DB
*/
func (context Context) GetConnection() (*sql.DB, error) {
	logrus.Debug("GetConnection")
	logrus.WithField("connectionString", context.connectionString).Debug("GetConnection")
	//Open connection to db
	var db, err = sql.Open("postgres", context.connectionString)
	if err != nil {
		logrus.WithError(err).Error("GetConnection Error connect to db")
		return nil, err
	}
	//Check connection to DB
	err = db.Ping()
	if err != nil {
		logrus.WithError(err).Error("GetConnection Error ping db")
		return nil, err
	}
	return db, nil
}

/*
Helper for execute query with connection
*/
func (context Context) Using(query DBContextFunction) error {
	var connection, err = context.GetConnection()
	if err != nil {
		return err
	}
	defer connection.Close()
	return query(connection)
}

/*
Helper for execute query with transaction
*/
func (context Context) UsingWithTransaction(query DBContextWithTransactionFunction) error {
	var connection, err = context.GetConnection()
	if err != nil {
		return err
	}
	defer connection.Close()
	//Start transaction
	transaction, err := connection.Begin()
	if err != nil {
		logrus.WithError(err).Error("can't open transaction")
		return err
	}
	//Rollback transaction on exit
	defer transaction.Rollback()

	//Execute query
	err = query(transaction)
	if err == nil {
		transaction.Commit()
		return nil
	}
	return err
}

/**
Execute query what return only one row
*/
func (context Context) GetSingleRow(query string, mapping DBFieldMapping, params ...interface{}) (data interface{}, err error) {
	data = nil
	err = context.Using(func(connection *sql.DB) error {
		result, pErr := mapping(connection.QueryRow(query, params...))
		if pErr == sql.ErrNoRows {
			return nil
		}
		if pErr != nil {
			logrus.WithError(pErr).WithFields(logrus.Fields{"query": query, "params": params}).Error("GetSingleRow")
			return err
		}
		data = result
		return nil
	})
	return
}

/**
Execute query
*/
func (context Context) ExecuteQuery(query string, params ...interface{}) error {
	return context.UsingWithTransaction(func(transaction *sql.Tx) error {
		var _, err = transaction.Exec(query, params...)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{"query": query, "params": params}).Error("ExecuteQuery")
		}
		return err
	})
}
