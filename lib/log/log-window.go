// +build windows

package log

import (
	"github.com/sirupsen/logrus"
	"github.com/mattn/go-colorable"
)

//Init log settings
func InitLog() {
	// Setup color for output
	logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
	logrus.SetOutput(colorable.NewColorableStdout())
}