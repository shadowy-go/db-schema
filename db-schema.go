package main

import (
	"flag"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy-go/db-schema/lib/log"
)

//command for execution
var flagCommand = flag.String("command", "export", "")

/**
Start point
Execute commands import, export
 */
func main() {
	log.InitLog()
	logrus.Info("db-schema")
	flag.Parse()
	logrus.WithField("command", *flagCommand).Debug("Execute")
}
