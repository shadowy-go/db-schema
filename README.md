#Db-Schema
##Server
version 1.0.0

[![pipeline status](https://gitlab.com/shadowy-go/db-schema/badges/master/pipeline.svg)](https://gitlab.com/shadowy-go/db-schema/commits/master)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/gate?key=server)](https://sonar.shadowy.eu/dashboard/index/db-schema)

[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=lines)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=comment_lines_density)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=function_complexity)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=blocker_violations)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=critical_violations)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=code_smells)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=bugs)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=vulnerabilities)](https://sonar.shadowy.eu/dashboard/index/db-schema)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=server&metric=sqale_debt_ratio)](https://sonar.shadowy.eu/dashboard/index/db-schema)